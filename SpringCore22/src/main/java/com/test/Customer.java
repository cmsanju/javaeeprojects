package com.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Customer {
	
	private int id;
	
	private String name;
	
	private String email;
	
	@Autowired
	@Qualifier("ord")
	private Order ordr;
	
	public Customer()
	{
		
	}


	public Customer(int id, String name, String email, Order ordr) {
		
		this.id = id;
		this.name = name;
		this.email = email;
		
		this.ordr = ordr;
	}
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name+" Email : "+email);
		
		ordr.details();
	}

}
