package com.test;

public class Order {
	
	private int id;
	
	private String name;
	
	private int qty;
	
	public Order()
	{
		
	}

	public Order(int id, String name, int qty) {
		
		this.id = id;
		this.name = name;
		this.qty = qty;
	}
	
	public void details()
	{
		System.out.println("ID : "+id+" Name : "+name+" Quantity : "+qty);
	}

}
