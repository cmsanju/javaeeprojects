package com.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfg {
	
	@Bean
	public Employee empObj()
	{
		return new Employee();
	}
	
	@Bean
	public Address adrObj()
	{
		return new Address();
	}

}
