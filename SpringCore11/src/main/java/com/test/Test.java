package com.test;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Test {
	
	public static void main(String[] args) {
		
		//Resource rs = new ClassPathResource("beans.xml");
		
		//BeanFactory bn = new XmlBeanFactory(rs);
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfg.class);
		
		Employee e = (Employee)ctx.getBean(Employee.class);
		
		Address a = (Address)ctx.getBean(Address.class);
		
		a.setStreet("JP nagar");
		a.setCity("Blr");
		a.setState("KA");
		
		e.setId(101);
		e.setName("Devesh");
		e.setCmp("Dell");
		
		e.setAdr(a);
		
		e.disp();
	}

}

//Repository https://gitlab.com/cmsanju/javaeeprojects.git










