package com.test;

public class Address {
	
	private String city;
	
	private String state;
	
	private String cnty;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCnty() {
		return cnty;
	}

	public void setCnty(String cnty) {
		this.cnty = cnty;
	}
	
	public void details()
	{
		System.out.println("City : "+city+" State : "+state+" Country : "+cnty);
	}

}
