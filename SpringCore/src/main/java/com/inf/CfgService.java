package com.inf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CfgService {
	
	@Bean
	public TextFormat textFormat()
	{
		return new TextFormat();
	}
	
	@Bean
	public NumFormat numFormat()
	{
		return new NumFormat();
	}
}
