package com.inf;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		/*
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		TextFormat tf = ctx.getBean("textFormat", TextFormat.class);
		
		System.out.println(tf.format());
		
		NumFormat nf = ctx.getBean("numFormat", NumFormat.class);
		
		System.out.println(nf.format());
		
		
		FormatService obj = ctx.getBean("service1", FormatService.class);
		*/
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(CfgService.class);
		
		TextFormat tf =  ctx.getBean(TextFormat.class);
		
		FormatService fs = new FormatService();
		
		fs.setFormatInf(tf);
		
		fs.disp();
		
		NumFormat nf = ctx.getBean(NumFormat.class);
		
		FormatService fs1 = new FormatService();
		
		fs1.setFormatInf(nf);
		
		fs1.disp();
	}
}
