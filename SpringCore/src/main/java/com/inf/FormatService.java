package com.inf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class FormatService {

	@Autowired
	//@Qualifier("numFormat")
	private FormatInf formatInf;
	
	
	
	public void setFormatInf(FormatInf formatInf) {
		this.formatInf = formatInf;
	}



	public void disp()
	{
		System.out.println(formatInf.format());
	}
}
