package com.inf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component//("textFormat")
@Qualifier("textFormat")
public class TextFormat implements FormatInf
{

	public String format() {
		
		return "Text Format";
	}

}
