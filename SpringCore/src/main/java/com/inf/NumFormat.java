package com.inf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component//("numFormat")
@Qualifier("numFormat")
public class NumFormat implements FormatInf
{

	@Override
	public String format() {
		
		return "Num Format";
	}

}
