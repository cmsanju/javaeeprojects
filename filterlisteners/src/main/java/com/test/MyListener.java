package com.test;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


@WebListener
public class MyListener implements HttpSessionListener {

	public static ServletContext ctx = null;
	
	public static int total = 0 ,current = 0;
	
    public void sessionCreated(HttpSessionEvent se)  { 
         total++;
         current++;
         
         ctx = se.getSession().getServletContext();
         
         ctx.setAttribute("totalusers", total);
         ctx.setAttribute("currentusers", current);
    }

	
    public void sessionDestroyed(HttpSessionEvent se)  { 
    	
    	current --;
    	
    	ctx.setAttribute("currentusers", current);
         
    }
	
}
