package com.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	
	@RequestMapping("/home")
	public String homePage()
	{
		return "landing";
	}
	
	@RequestMapping("/hello")
	public String sayHelloAgain()
	{
		return "again";
	}
}
