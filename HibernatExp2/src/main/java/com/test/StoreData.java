package com.test;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class StoreData {
	
	public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();
		
		Session session = sf.openSession();
		
		Transaction t = session.beginTransaction();
		
		Student sobj = new Student();
		
		sobj.setStd_name("Animesh");
		sobj.setStd_clg("IITS");
		
		//insert / create student
		session.save(sobj);
		
		//read student
		//Student s = (Student)session.get(Student.class, 2);
		//update student
		//s.setStd_clg("IIIT");
		
		//System.out.println(s.getId()+" "+s.getStd_name()+" "+s.getStd_clg());
		//delete student
		//session.delete(s);
		
		//Read all the records
		Query qry = session.createQuery("from Student");
		
		List<Student> sList = qry.list();
		
		for(Student s1 : sList)
		{
			System.out.println(s1.getId()+" "+s1.getStd_name()+" "+s1.getStd_clg());
		}
		
		t.commit();
		
		session.close();
		
		System.out.println("Done.");
	}

}
