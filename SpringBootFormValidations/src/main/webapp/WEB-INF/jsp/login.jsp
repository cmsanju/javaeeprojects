<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<style type="text/css">
	.error{
		color : red;
		}
</style>

</head>
<body>
<form:form action="usersubmit" method="post" modelAttribute = "user">
<!-- 
		<h1>login page</h1>
		
		Enter UserName : <input type = "text" name = "userName"><br><br>
		Enter Password : <input type = "password" name = "password"><br><br>
		
				<input type = "submit" value="login">
 -->
 		<div>
 		<form:label path="userName">Enter UserName</form:label>			
 		 <form:input path="userName"/>
 				<form:errors path="userName" cssClass="error"/>
 		</div>
 		<div>
 		<form:label path="password">Enter Password</form:label>
 		 <form:input path="password"/>
 						<form:errors path="password" cssClass="error" />
 		</div>
 				<input type = "submit" value="login">
	</form:form>
</body>
</html>