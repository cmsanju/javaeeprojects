package com.test.model;

import javax.validation.constraints.NotEmpty;

public class User {
	
	@NotEmpty(message = "user required")
	private String userName;
	
	@NotEmpty(message = "password required")
	private String password;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
