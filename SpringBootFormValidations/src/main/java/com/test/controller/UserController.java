package com.test.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.test.model.User;

@Controller
public class UserController {
	
	@RequestMapping("/")
	public String homePage()
	{
		return "home";
	}
	
	@RequestMapping("/userlogin")
	public String loginPage(Model model, User user)
	{
		model.addAttribute("user", user);
		
		return "login";
	}
	
	@RequestMapping(value = "/usersubmit", method = RequestMethod.POST)
	public String submitLoginForm(@Valid @ModelAttribute("user") User usr,
			BindingResult bindingResult)
	{
		if(bindingResult.hasErrors())
		{
			return "login";
		}
		else
		{
			return "success";
		}
	}
}
