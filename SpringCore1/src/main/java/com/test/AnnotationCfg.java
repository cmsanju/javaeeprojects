package com.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AnnotationCfg {
	
	@Bean
    public Car car(Engine engine) {
        return new Car(engine);
    }
    
    @Bean
    public Engine engine(CamShaft camshaft, CrankShaft crankshaft) {
        return new CombustionEngine(camshaft, crankshaft);
    }
    
    @Bean
    public CamShaft camshaft() {
        return new CamShaft();
    }
    
    @Bean
    public CrankShaft crankshaft() {
        return new CrankShaft();
    }
}
