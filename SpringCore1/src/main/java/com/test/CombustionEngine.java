package com.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class CombustionEngine implements Engine
{
	
	private CamShaft camshaft;
    private CrankShaft crankshaft;

    @Autowired
    public CombustionEngine(CamShaft camshaft, CrankShaft crankshaft) {
        this.camshaft = camshaft;
        this.crankshaft = crankshaft;
    }
    //@Override
	public void startEngine() {
		
		System.out.println("Satarted Combustion Engine");
	}

    
}
