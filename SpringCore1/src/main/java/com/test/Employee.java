package com.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

public class Employee {
	
	private int id;
	
	private String name;
	
	private List<String> cmp;
	
	@Autowired
	private Address adr;
	
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, List<String> cmp, Address adr)
	{
		this.id = id;
		this.name = name;
		this.cmp = cmp;
		this.adr = adr;
	}
	
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name+" Company : "+cmp);
		
		adr.details();
	}
}
