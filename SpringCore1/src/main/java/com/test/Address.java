package com.test;

public class Address {
	
	private String city;
	
	private String state;
	
	private int pincode;
	
	
	public Address()
	{
		
	}
	
	public Address(String city, String state, int pincode)
	{
		this.city = city;
		this.state = state;
		this.pincode = pincode;
	}
	
	public void details()
	{
		System.out.println("City : "+city+" State : "+state+" Pincode : "+pincode);
	}
}
