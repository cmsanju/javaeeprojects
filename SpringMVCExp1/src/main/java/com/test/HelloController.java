package com.test;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloController {
	
	@RequestMapping("/homepage")
	public String homePage()
	{
		return "home";
	}
	
	@RequestMapping("/login")
	public String userLogin()
	{
		return "userlogin";
	}
	
	@RequestMapping("/register")
	public String userRegister()
	{
		return "userreg";
	}
	/*
	@RequestMapping("/submit")
	public String userLoginSubmit(HttpServletRequest request, Model model)
	{
		String usr = request.getParameter("user");
		String pass = request.getParameter("pwd");
		
		User usr1 = new User();
		
		usr1.setUserName(usr);
		usr1.setPassword(pass);
		
		model.addAttribute("info",usr1);
		
		return "loginsuccess";
	}
	*/
	
	@RequestMapping("/submit")
	public String userLoginSubmit(@RequestParam("user")String usr, @RequestParam("pwd")String pass, Model model)
	{
		//String usr = request.getParameter("user");
		//String pass = request.getParameter("pwd");
		
		User usr1 = new User();
		
		usr1.setUserName(usr);
		usr1.setPassword(pass);
		
		model.addAttribute("info",usr1);
		
		return "loginsuccess";
	}
	
	@RequestMapping("/regsubmit")
	public String userRegSubmit(HttpServletRequest request, Model model)
	{
		User usr = new User();
		
		String fname = request.getParameter("fname");
		String usr1 = request.getParameter("user");
		String pass = request.getParameter("pwd");
		
		usr.setfName(fname);
		usr.setUserName(usr1);
		usr.setPassword(pass);
		
		model.addAttribute("info", usr);
		
		return "regsuccess";
	}

}
