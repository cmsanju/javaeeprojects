package com.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmpService empService;

	public void setEmpService(EmpService empService) {
		this.empService = empService;
	}
	
	@GetMapping(value = "/employees", produces = "application/json")
	public Employees getAllEmployees()
	{
		return empService.getAllEmployees();
	}
	
	@PostMapping(value = "/addemployee", consumes = "application/json")
	public void addEmployee(@RequestBody Employee emp)
	{
		Integer id = empService.getAllEmployees().getEmpList().size() + 1;
		
		emp.setId(id);
		
		System.out.println(emp.getName());
		
		empService.saveEmp(emp);
	}
	
	@PutMapping(value = "/updateEmployee/{id}", consumes = "application/json")
	@ResponseBody
	public Employees updateEmployee(@RequestBody Employee emp, @PathVariable("id") Integer id)
	{
		emp.setId(id);
		
		empService.updateEmp(emp);
		
		
		return empService.getAllEmployees();
	}
	@DeleteMapping(value = "/deleteEmployee/{id}", produces = "application/json")
	public Employees deeletEmployee(@PathVariable("id") Integer id)
	{
		empService.deleteEmp(id);
		
		return empService.getAllEmployees();
	}
}
