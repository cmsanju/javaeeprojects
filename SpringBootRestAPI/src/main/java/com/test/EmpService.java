package com.test;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public class EmpService {
	
	private static Employees listEmp = new Employees();
	
	static
	{
		listEmp.getEmpList().add(new Employee(101, "Ram", "IBM", "Blr"));
		listEmp.getEmpList().add(new Employee(102, "King", "PWC", "hyd"));
		listEmp.getEmpList().add(new Employee(103, "Apple", "E Y", "blr"));
		listEmp.getEmpList().add(new Employee(104, "Rakesh", "wipro", "hyd"));
		listEmp.getEmpList().add(new Employee(105, "Ranjan", "JSW", "Chn"));
	}
	//create / insert employee
	public void saveEmp(Employee emp)
	{
		listEmp.getEmpList().add(emp);
	}
	
	//read / select / list all employees
	public Employees getAllEmployees()
	{
		return listEmp;
	}
	
	//update employee
	public String updateEmp(Employee emp)
	{	
		for(int i = 0; i<listEmp.getEmpList().size(); i++)
		{
			Employee emp1 = listEmp.getEmpList().get(i);
			
			if(emp1.getId().equals(emp.getId()))
			{
				listEmp.getEmpList().set(i, emp);
			}
		}
		
		return  "the given id is not available";
	}
	
	//delete employee
	public String deleteEmp(Integer id)
	{
		for(int i = 0 ; i< listEmp.getEmpList().size(); i++)
		{
			Employee emp = listEmp.getEmpList().get(i);
			
			if(emp.getId().equals(id))
			{
				listEmp.getEmpList().remove(i);
			}
		}
		
		return "the given id is not available";
	}
}
