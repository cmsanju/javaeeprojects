package com.test;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloController {
	
	@RequestMapping("/home")
	public String homePage()
	{
		return "homepage";
	}
	
	@RequestMapping("/login")
	public String loginPage()
	{
		return "loginpage";
	}
	
	@RequestMapping("/register")
	public String registerPage()
	{
		return "regpage";
	}
	/*
	@RequestMapping("/submitform")
	public String submitLoginForm(HttpServletRequest request, Model model)
	{
		String usr = request.getParameter("user");
		
		String pass = request.getParameter("pwd");
		
		User u = new User();
		
		u.setUserName(usr);
		u.setPassword(pass);
		
		//String dt = u.getUserName()+" "+u.getPassword();
		
		model.addAttribute("info", u);
		
		return "details";
	}
	
	*/
	
}
