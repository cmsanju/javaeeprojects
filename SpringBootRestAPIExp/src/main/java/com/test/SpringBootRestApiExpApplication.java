package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestApiExpApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestApiExpApplication.class, args);
	}

}
