package com.test.service;


import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.test.model.Student;
import com.test.model.Students;

@Repository
@Service
public class StudentService {
		
	private static Students stdData = new Students();
	
	static
	{
		stdData.getStdList().add(new Student(101, "Ramanuj", "BITS", "Blr"));
		
		stdData.getStdList().add(new Student(102, "Vdas", "MITS", "MPL"));
		
		stdData.getStdList().add(new Student(103, "Rakesh", "VITS", "Vellore"));
		
		stdData.getStdList().add(new Student(104, "Ranjan", "IITS", "Blr"));
		
		stdData.getStdList().add(new Student(105, "Rohit", "JAIN", "Pune"));
		
	}
	
	//displaying all student data
	public Students getAllStudents()
	{
		return stdData;
	}
	
	//create student data
	public void addSutdentRecord(Student obj)
	{
		stdData.getStdList().add(obj);
	}
	
	//update student data
	public String updateStudentData(Student obj)
	{
		for(int i = 0; i < stdData.getStdList().size(); i++)
		{
			Student std = stdData.getStdList().get(i);
			
			if(std.getId().equals(obj.getId()))
			{
				stdData.getStdList().set(i, obj);
			}
		}
		
		return "the given id is not available";
	}
	
	//Delete student data
	public String deleteStudentData(Integer id)
	{
		for(int i = 0; i < stdData.getStdList().size(); i++)
		{
			Student std = stdData.getStdList().get(i);
			
			if(std.getId().equals(id))
			{
				stdData.getStdList().remove(i);
			}
		}
		
		return "the given id is not available";
	}
}
