package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.model.Student;
import com.test.model.Students;
import com.test.service.StudentService;

@RestController
public class StudentController {
	
	@Autowired
	private StudentService service;
	
	@GetMapping( value = "/studentlist", produces = "application/json")
	public Students getAllStudents()
	{
		return service.getAllStudents();
	}
	
	@PostMapping(value = "/addstudent", consumes = "application/json")
	public Students addStudent(@RequestBody Student obj)
	{
		int id = service.getAllStudents().getStdList().size() + 1;
		
		obj.setId(id);
		
		service.addSutdentRecord(obj);
		
		return service.getAllStudents();
	}
	
	@PutMapping(value = "/updatestudent/{id}", consumes = "application/json")
	public Students updateStudent(@RequestBody Student obj, @PathVariable("id") Integer id)
	{
		obj.setId(id);
		
		service.updateStudentData(obj);
		
		return service.getAllStudents();
	}
	
	@DeleteMapping(value = "/deletestudent/{id}", produces = "application/json")
	@ResponseBody
	public Students deleteStudent(@PathVariable("id") Integer id)
	{
		service.deleteStudentData(id);
		
		return service.getAllStudents();
	}

}
