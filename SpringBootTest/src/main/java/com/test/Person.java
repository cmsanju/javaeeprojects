package com.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
 
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
		
	@Id
    private Integer personId;
    private String personName;
    private String personCity;
    
    public Person()
    {
    	
    }
    
	public Person(Integer personId, String personName, String personCity) {
		
		this.personId = personId;
		this.personName = personName;
		this.personCity = personCity;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getPersonCity() {
		return personCity;
	}
	public void setPersonCity(String personCity) {
		this.personCity = personCity;
	}
    
}
