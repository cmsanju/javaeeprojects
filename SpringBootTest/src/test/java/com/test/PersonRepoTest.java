package com.test;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
 
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
public class PersonRepoTest {
	
	@Autowired
    private PersonRepo personRepo;
 
    @Test
    void isPersonExitsById() {
        Person person = new Person(1002, "Perfs", "Blr");
        personRepo.save(person);
        Boolean actualResult = personRepo.isPersonExitsById(1002);
        assertThat(actualResult).isTrue();
    }
    
    @Test
    void isPersonNotExitsById() {
        //Person person = new Person(1002, "Perfs", "Blr");
        //personRepo.save(person);
        Boolean actualResult = personRepo.isPersonExitsById(1001);
        assertThat(actualResult).isFalse();
    }

}
