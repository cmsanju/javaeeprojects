package com.test;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

public class EmpDao {
	
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public int save(Employee emp)
	{
		String sql = "insert into emp values("+emp.getId()+", '"+emp.getName()+"','"+emp.getCmp()+"','"+emp.getLoc()+"')";
		
		return jdbcTemplate.update(sql);
	}
	
	public int update(Employee emp)
	{
		String sql = "update emp set emp_name = '"+emp.getName()+"', emp_cmp = '"+emp.getCmp()+"', emp_loc = '"+emp.getLoc()+"' where id = "+emp.getId()+" ";
		
		return jdbcTemplate.update(sql);
	}
	
	public int delete(Employee emp)
	{
		String sql = "delete from emp where id = "+emp.getId()+" ";
		
		return jdbcTemplate.update(sql);
	}
	
	public List<Employee> getEmployees()
	{
		return jdbcTemplate.query("select * from emp", new ResultSetExtractor<List<Employee>>()
				{
					public List<Employee> extractData(ResultSet rs)throws SQLException,DataAccessException
					{
						List<Employee> listEmp = new ArrayList<Employee>();
						
						while(rs.next())
						{
							Employee emp = new Employee();
							
							emp.setId(rs.getInt(1));
							emp.setName(rs.getString(2));
							emp.setCmp(rs.getString(3));
							emp.setLoc(rs.getString(4));
							
							listEmp.add(emp);
						}
						
						return listEmp;
					}
				});
	}
}
