package com.test;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		EmpDao obj = ctx.getBean("edao", EmpDao.class);
		
		Employee e = new Employee();
		
		e.setId(105);
		e.setName("Saurav");
		e.setCmp("PWC");
		e.setLoc("blr");
		
		//obj.save(e);
		
		//obj.update(e);
		
		//obj.delete(e);
		
		List<Employee> listEmp = obj.getEmployees();
		
		for(Employee eobj : listEmp)
		{
			System.out.println("ID : "+eobj.getId()+" Name : "+eobj.getName()+" Company: "+eobj.getCmp()+" Location : "+eobj.getLoc());
		}
		
		System.out.println("Done.");
	}

}

//Repository https://gitlab.com/cmsanju/javaeeprojects.git






