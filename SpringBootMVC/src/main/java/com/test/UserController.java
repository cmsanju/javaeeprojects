package com.test;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {
	
	@RequestMapping("/submitform")
	public String submitLoginForm(@RequestParam("user") String user, @RequestParam("pwd") String pass, Model model)
	{
		User u = new User();
		
		u.setUserName(user);
		u.setPassword(pass);
		
		model.addAttribute("info", u);
		
		return "details";
	}

}
