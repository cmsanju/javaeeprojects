package com.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	
	@RequestMapping("/")
	public String homePage()
	{
		return "home";
	}
	
	@RequestMapping("/greet")
	public String greetUser()
	{
		return "greetings";
	}
	
	@RequestMapping("/again")
	public String greetAgain()
	{
		return "greetagain";
	}
	
	@RequestMapping("/login")
	public String loginPage()
	{
		return "loginpage";
	}
	
	@RequestMapping("/register")
	public String registerPage()
	{
		return "regpage";
	}
}
