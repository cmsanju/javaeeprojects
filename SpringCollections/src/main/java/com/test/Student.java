package com.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Student {
	
	private int id;
	
	private String name;
	
	private List<String> course;
	
	private List<Colleges> clg;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getCourse() {
		return course;
	}

	public void setCourse(List<String> course) {
		this.course = course;
	}
	
	public List<Colleges> getClg() {
		return clg;
	}

	public void setClg(List<Colleges> clg) {
		this.clg = clg;
	}
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name);
		
		System.out.println("Course : ");
		
		Iterator<String> itr = course.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		//clg = new ArrayList<Colleges>();
		
		for(Colleges cl : clg)
		{
			System.out.println(cl.getName()+" "+cl.getLoc());
		}
	}

	
}
