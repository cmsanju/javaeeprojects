package com.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Map<String, Integer> data = new HashMap<>();
		
		data.put("lenovo", 200);
		data.put("dell", 300);
		data.put("sony", 400);
		data.put("mac", 500);
		data.put("lap", 43);
		data.put("lenovo", 500);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> dt = itr.next();
			
			System.out.println("Product : "+dt.getKey()+" Value : "+dt.getValue());
			
		}
		
		for(String k : data.keySet())
		{
			System.out.println(k+" "+data.get(k));
		}
		
		data.forEach((k,v) -> System.out.println(k+" : "+v));
	}

}
