package com.test;

public class Colleges {
	
	private String name;
	
	private String loc;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}
	
	public void details()
	{
		System.out.println("Name : "+name+" Location : "+loc);
	}
}
