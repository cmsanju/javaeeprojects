package com.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Exp1 {
	
	public static void main(String[] args) {
		
		List<String> data = new ArrayList<>();
		
		data.add("lenovo");
		data.add("dell");
		data.add("sony");
		data.add("mac");
		data.add("lenovo");
		data.add("apple");
		
		
		Iterator<String> itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		for(String str : data)
		{
			System.out.println(str);
		}
		
		data.forEach(x -> System.out.println(x));
	}
}
