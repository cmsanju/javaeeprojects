package com.test;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component//("textMsg")
@Qualifier("textMsg")
//@Primary
public class TextMsg implements MessageService
{

	public String message() {
		
		return "This is simple text service";
	}
	
}
