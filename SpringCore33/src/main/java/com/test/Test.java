package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
	public static void main(String[] args) {
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(CfgService.class);
		
		TextMsg tmsg = (TextMsg) ctx.getBean(TextMsg.class);
		
		
		
		EmailMsg emsg = (EmailMsg)ctx.getBean(EmailMsg.class);
		
		
		ServiceExp obj = new ServiceExp();
		
		obj.setMessageService(emsg);
		
		obj.details();
	}
}
