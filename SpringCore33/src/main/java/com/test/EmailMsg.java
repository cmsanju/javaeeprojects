package com.test;

import org.springframework.stereotype.Component;

@Component
public class EmailMsg implements MessageService
{
	public String message() {
		
		return "Simple email service";
	}
	
}
