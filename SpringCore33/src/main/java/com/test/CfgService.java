package com.test;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CfgService {
		
	@Bean(autowire = Autowire.BY_NAME)
	public TextMsg txtMsg1()
	{
		return new TextMsg();
	}
	
	@Bean
	public EmailMsg emailMsg()
	{
		return new EmailMsg();
	}
}
