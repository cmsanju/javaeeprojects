package com.test;

import org.springframework.beans.factory.annotation.Autowired;

public class ServiceExp {
	
	@Autowired
	private MessageService messageService;

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}
	
	public void details()
	{
		System.out.println(messageService.message());
	}

}
