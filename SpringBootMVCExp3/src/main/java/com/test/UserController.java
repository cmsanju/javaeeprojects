package com.test;

//import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {
	
	@RequestMapping("login")
	public String loginPage()
	{
		return "userlogin";
	}
	/*
	@RequestMapping("submituser")
	public String submitLogin(HttpServletRequest request, Model model)
	{
		String usr = request.getParameter("user");
		String pwd = request.getParameter("pwd");
		
		User us = new User();
		
		us.setUserName(usr);
		us.setPassword(pwd);
		
		String dt = usr+" "+pwd;
		
		model.addAttribute("data", us);
		
		return "details";
	}
	
	*/
	
	@RequestMapping("/submituser")
	public String submitLogin(@RequestParam("user") String usr, @RequestParam("pwd") String pwd, Model model)
	{
		User obj = new User();
		
		obj.setUserName(usr);
		obj.setPassword(pwd);
		
		model.addAttribute("data", obj);
		
		return "details";
	}
}
